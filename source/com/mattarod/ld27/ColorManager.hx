package com.mattarod.ld27;

import flixel.FlxG;

class ColorManager {
	
	public inline static var opaque_mask = 0xFF000000;
	
	public inline static var opaque_flag = 0xFF;
	
	public inline static var BLACK:Int = 0x000000;
	public inline static var WHITE:Int = 0xFFFFFF;
	public inline static var BLUE:Int = 0x0000FF;
	public inline static var RED:Int = 0xFF0000;
	
	public inline static var CLEAR:Int = 0x00000000;
	
	public inline static var BLACK_OPAQUE:Int = 0xFF000000;
	public inline static var WHITE_OPAQUE:Int = 0xFFFFFFFF;
	public inline static var BLUE_OPAQUE:Int = 0xFF0000FF;
	public inline static var RED_OPAQUE:Int = 0xFFFF0000;
	
	public static function rgbToOpaqueRGBA(color:Int):Int {
		return color + opaque_mask;
	}
	
	public static function setBackgroundColorRGB(color:Int):Void {
		FlxG.cameras.bgColor = rgbToOpaqueRGBA(color);
	
	}
	
	public static function setBackgroundColorRGBA(color:Int):Void {
		FlxG.cameras.bgColor = color;
	}
	
	public static function makeRGBA(rgb:Int, a:Int) {
		return rgb + 0x1000000 * a;
	}
}
