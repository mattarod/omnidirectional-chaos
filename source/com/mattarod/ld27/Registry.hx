package com.mattarod.ld27;

import com.mattarod.ld27.gameStates.GameState;

class Registry {
	public static var gameState:GameState;
	
	public static var deaths:Int = 0;
	public static var bestTimeInHundrethsOfSeconds:Int = 0;
	public static var muteMusic:Bool = false;
}
