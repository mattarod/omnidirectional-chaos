package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;
import flixel.util.FlxPoint;

class SetPositionAction implements ScriptAction {

	private var position:FlxPoint;
	
	public function new(position:FlxPoint) {
		this.position = position;
	}
	
	public function act(enemy:EnemySprite) {
		enemy.x = position.x + enemy.offset.x;
		enemy.y = position.y + enemy.offset.y;
	}
	
}
