package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;
import com.mattarod.ld27.gameObjects.scripts.ScriptAction;

class WildAction implements ScriptAction {

	private var func:EnemySprite->Void;
	
	public function new(func:EnemySprite-> Void) {
		this.func = func;
	}
	
	public function act(enemy:EnemySprite) {
		func(enemy);
	}
}
