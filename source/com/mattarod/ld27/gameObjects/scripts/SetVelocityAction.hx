package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;
import com.mattarod.ld27.Registry;
import flixel.util.FlxPoint;

class SetVelocityAction implements ScriptAction {

	private var velocity:FlxPoint;
	
	public function new(velocity:FlxPoint) {
		this.velocity = velocity;
	}
	
	public function act(enemy:EnemySprite) {
		enemy.velocity.x = velocity.x;
		enemy.velocity.y = velocity.y;
	}
}
