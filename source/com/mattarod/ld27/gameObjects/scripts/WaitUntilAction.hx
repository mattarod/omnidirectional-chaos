package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;

class WaitUntilAction extends WaitAction {

	public var time:Float;
	
	public function new(time:Float) {
		super(0);
		
		this.time = time;
	}

	public override function act(enemy:EnemySprite):Void {
		duration = time - Registry.gameState.time;
		
		if (duration < 0) {
			duration = 0;
		}
	}
}
