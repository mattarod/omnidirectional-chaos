package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;

interface ScriptAction {
	public function act(parent:EnemySprite):Void;
}
