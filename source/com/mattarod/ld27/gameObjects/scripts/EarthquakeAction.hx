package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;
import com.mattarod.ld27.gameObjects.scripts.ScriptAction;
import com.mattarod.ld27.Registry;

class EarthquakeAction implements ScriptAction {

	public var intensity:Float;
	public var duration:Float;
	
	public function new(intensity:Float, duration:Float = 1) {
		this.intensity = intensity;
		this.duration = duration;
	}
	
	public function act(enemy:EnemySprite) {
		Registry.gameState.earthquake(intensity, duration);
	}
}
