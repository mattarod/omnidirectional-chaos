package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;
import com.mattarod.ld27.gameObjects.scripts.ScriptAction;
import com.mattarod.ld27.Registry;

class SleepAction extends WaitAction {
	
	public function new(duration:Float) {
		super(duration);
	}

	public override function act(enemy:EnemySprite):Void {
		enemy.exists = false;
		enemy.sleeping = true;
		
		Registry.gameState.enemyGroup.addSleepingMember(enemy);
		
		return;
	}
}
