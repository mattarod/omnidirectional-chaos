package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.scripts.ScriptAction;

class SetDeadlyAction implements ScriptAction {

	private var deadly:Bool;
	
	public function new(deadly:Bool) {
		this.deadly = deadly;
	}
	
	public function act(enemy:EnemySprite) {
		enemy.deadly = deadly;
	}
}
