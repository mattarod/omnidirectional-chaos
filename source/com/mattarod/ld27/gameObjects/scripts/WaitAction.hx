package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;

class WaitAction implements ScriptAction {

	public var duration:Float;
	
	public function new(duration:Float) {
		this.duration = duration;
	}

	public function act(enemy:EnemySprite):Void {
		// Do nothing.
		return;
	}
}
