package com.mattarod.ld27.gameObjects.scripts;

class Script {
	
	public static inline var FOREVER:Float = -1;
	public static inline var INSTANT:Float = 0;
	
	private var actions:Array<ScriptAction>;
	private var index:Int = 0;
	
	public function new() {
		actions = new Array<ScriptAction>();
	}
	
	public function addAction(action:ScriptAction):Void {
		actions.push(action);
	}
	
	public function nextAction():ScriptAction {
		if (actions.length == 0) {
			return null;
		}
		
		var currentIndex = index;
		
		index++;
		
		if (index >= actions.length) {
			index = 0;
		}
		
		var nextAction:ScriptAction = actions[currentIndex];
		
		return nextAction;
	}
}
