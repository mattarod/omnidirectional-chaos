package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.scripts.ScriptAction;
import flixel.FlxG;

class PlaySoundAction implements ScriptAction {

	private var sound:String;
	
	public function new(sound:String) {
		this.sound = sound;
	}
	
	public function act(enemy:EnemySprite) {
		FlxG.sound.play(sound);
	}
	
}
