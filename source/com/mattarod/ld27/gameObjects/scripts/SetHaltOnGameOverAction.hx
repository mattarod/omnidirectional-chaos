package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.scripts.ScriptAction;

class SetHaltOnGameOverAction  implements ScriptAction {

	private var haltOnGameOver:Bool;
	
	public function new(haltOnGameOver:Bool) {
		this.haltOnGameOver = haltOnGameOver;
	}
	
	public function act(enemy:EnemySprite) {
		enemy.haltOnGameOver = haltOnGameOver;
	}
}
