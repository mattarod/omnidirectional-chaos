package com.mattarod.ld27.gameObjects.scripts;

import com.mattarod.ld27.gameObjects.EnemySprite;

class SleepUntilAction extends SleepAction {

	public var time:Float;
	
	public function new(time:Float) {
		super(0);
		
		this.time = time;
	}

	public override function act(enemy:EnemySprite):Void {
		duration = time - Registry.gameState.time;
		
		if (duration < 0) {
			duration = 0;
		}
		
		super.act(enemy);
	}
}
