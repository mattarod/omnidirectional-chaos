package com.mattarod.ld27.gameObjects;

import com.mattarod.ld27.ColorManager;
import com.mattarod.ld27.gameObjects.scripts.Script;
import com.mattarod.ld27.gameObjects.scripts.ScriptAction;
import com.mattarod.ld27.gameObjects.scripts.WaitAction;
import com.mattarod.ld27.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxPoint;

class EnemySprite extends FlxSprite {

	public static inline var TILE_LENGTH:Int = Constants.TILE_LENGTH;
	public static inline var NEVER:Float = -1;
	
	private var script:Script;
	private var currentAction:ScriptAction;
	private var nextActionTime:Float = NEVER;
	
	public var sleeping:Bool = false;
	public var deadly:Bool = true;
	public var haltOnGameOver:Bool = false;
	
	public function new(x:Float = 0, y:Float = 0, w:Int = TILE_LENGTH, h:Int = TILE_LENGTH, deadly:Bool = true, spriteName:String = null, script:Script = null) {
		super(x, y);
		
		if (spriteName == null) {
			var color = deadly ? ColorManager.RED_OPAQUE : ColorManager.BLACK_OPAQUE;
			
			makeGraphic(w, h, color);
		} else {
			loadGraphic(spriteName);
		}
		
		if (script == null) {
			script = new Script();
		}
		
		this.script = script;
		this.deadly = deadly;
		
		immovable = true;
		
		nextAction();
	}
	
	public function shrinkHitbox(top:Float, bottom:Float, left:Float, right:Float) {
		height -= top + bottom;
		width -= left + right;
		
		x += left;
		y += top;
		
		offset = new FlxPoint(left, top);
	}
	
	private function nextAction():Void {
		currentAction = script.nextAction();
		
		if (currentAction == null) {
			nextActionTime == NEVER;
			return;
		}
		
		while (!Std.is(currentAction, WaitAction)) {
			currentAction.act(this);
			currentAction = script.nextAction();
		}
		
		var waitAction:WaitAction = cast(currentAction);
		
		waitAction.act(this);
		
		// Load duration from currentAction.
		if (waitAction.duration == Script.FOREVER) {
			nextActionTime = NEVER;
		} else {
			nextActionTime = Registry.gameState.time + waitAction.duration;
		}
	}
	
	public function tryToAwaken():Bool {
		if (Registry.gameState.gameOver) {
			// Do nothing
			return false;
		}
		
		if (nextActionTime != NEVER && Registry.gameState.time >= nextActionTime) {
			exists = true;
			sleeping = false;
			return true;
		}
		
		return false;
	}
	
	public override function update():Void {
		if (nextActionTime != NEVER &&
			Registry.gameState.time + (FlxG.elapsed / 2) >= nextActionTime &&
			(!haltOnGameOver || !Registry.gameState.gameOver)) {
			nextAction();
		}
		
		super.update();
	}
}
