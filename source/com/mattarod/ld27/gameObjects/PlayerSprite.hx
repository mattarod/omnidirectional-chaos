package com.mattarod.ld27.gameObjects;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxPoint;

class PlayerSprite extends FlxSprite {
	
	public static inline var TILE_LENGTH:Int = Constants.TILE_LENGTH;
	public static inline var WALK_SPEED:Int = 8 * TILE_LENGTH;
	public static inline var FALL_ACCELERATION:Int = 40 * TILE_LENGTH;
	public static inline var JUMP_SPEED = -16 * TILE_LENGTH;
	
	private var state:PlayerState;
	
	public function new(x:Float, y:Float) {
		super(x+7, y+2);
		
		loadGraphic("png/hero.png", false);
		
		this.setFacingFlip(FlxObject.LEFT, true, false);
		this.setFacingFlip(FlxObject.RIGHT, false, false);
		
		height -= 3;
		width -= 14;
		
		offset = new FlxPoint(7, 3);
		
		acceleration.y = FALL_ACCELERATION;
		
		state = PlayerState.ON_LAND;
	}
	
	public override function update() {
		if (!alive) {
			super.update();
			return;
		}
		
		// Move horizontally
		var left:Bool = FlxG.keys.pressed.LEFT || FlxG.keys.pressed.A;
		var right:Bool = FlxG.keys.pressed.RIGHT || FlxG.keys.pressed.D;
		
		var jump:Bool = FlxG.keys.pressed.J || FlxG.keys.pressed.UP || FlxG.keys.pressed.W || FlxG.keys.pressed.X;
		var justJumped:Bool = FlxG.keys.justPressed.J || FlxG.keys.justPressed.UP || FlxG.keys.justPressed.W || FlxG.keys.justPressed.X;
		
		if (left && !right) {
			velocity.x = -WALK_SPEED;
			facing = FlxObject.LEFT;
		} else if (!left && right) {
			velocity.x = WALK_SPEED;
			facing = FlxObject.RIGHT;
		} else {
			velocity.x = 0;
		}
		
		// Jump
		if (justJumped && isTouching(FlxObject.FLOOR)) {
			velocity.y = JUMP_SPEED;
			FlxG.sound.play("Jump");
		}
		
		// Release jump
		if (!jump && velocity.y < 0) {
			velocity.y = 0;
		}
		
		if (Type.enumEq(state, PlayerState.IN_AIR) && isTouching(FlxObject.FLOOR)) {
			state = PlayerState.ON_LAND;
			FlxG.sound.play("Land");
		} else if (Type.enumEq(state, PlayerState.ON_LAND) && !isTouching(FlxObject.FLOOR) && velocity.y != 0) {
			state = PlayerState.IN_AIR;
		}
		
		super.update();
	}
	
}
