package com.mattarod.ld27.gameObjects;

import flixel.group.FlxTypedGroup;

class EnemyGroup extends FlxTypedGroup<EnemySprite> {

	private var sleepingEnemies:List<EnemySprite>;
	
	public function new() {
		super();
		
		sleepingEnemies = new List<EnemySprite>();
	}
	
	public function addSleepingMember(enemy:EnemySprite) {
		sleepingEnemies.add(enemy);
	}
	
	public override function update() {
		for (enemy in sleepingEnemies) {
			if (enemy.tryToAwaken()) {
				sleepingEnemies.remove(enemy);
			}
		}
		
		super.update();
	}
}
