package com.mattarod.ld27;

class DiscretePoint {
	public var x:Int;
	public var y:Int;
	
	public function new(x:Int, y:Int) {
		this.x = x;
		this.y = y;
	}
	
	public function equals(other:DiscretePoint) {
		return x == x && y == y;
	}
}
