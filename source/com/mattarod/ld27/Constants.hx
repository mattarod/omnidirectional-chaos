package com.mattarod.ld27;
import flixel.system.scaleModes.PixelPerfectScaleMode;

/**
 * ...
 * @author Matthew Rodriguez
 */
class Constants
{
	public static inline var TILE_LENGTH = 16;
	public static inline var GAME_WIDTH = 320;
	public static inline var GAME_HEIGHT = 240;
}