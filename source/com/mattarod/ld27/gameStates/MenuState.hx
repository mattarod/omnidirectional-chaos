package com.mattarod.ld27.gameStates;

import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.system.scaleModes.PixelPerfectScaleMode;

class MenuState extends FlxState {
	private static var setScaleMode:Bool = false;
	
	public override function create():Void {
		ColorManager.setBackgroundColorRGBA(ColorManager.WHITE_OPAQUE);
		
		if (!setScaleMode)
		{
			setScaleMode = true;
			FlxG.scaleMode = new PixelPerfectScaleMode();
		}
		
		FlxG.mouse.visible = false;
		
		var t1:FlxText = new FlxText(0, 20, Constants.GAME_WIDTH, "Omnidirectional Chaos");
		
		var t2:FlxText = new FlxText(0, 60, Constants.GAME_WIDTH, "By Matthew Rodriguez");
		var tm:FlxText = new FlxText(0, 70, Constants.GAME_WIDTH, "Music by Adan Ferguson");
		
		var t3:FlxText = new FlxText(0, 90, Constants.GAME_WIDTH, "Ludum Dare #27");
		var t4:FlxText = new FlxText(0, 100, Constants.GAME_WIDTH, "72-hour Jam Version");
		var t5:FlxText = new FlxText(0, 110, Constants.GAME_WIDTH, "August 25, 2013");
			
		var t6:FlxText = new FlxText(0, 130, Constants.GAME_WIDTH, "Move: Arrow Keys or WASD");
		var t7:FlxText = new FlxText(0, 140, Constants.GAME_WIDTH, "Jump: Up, W, X, or J");
		
		var t8:FlxText = new FlxText(0, 160, Constants.GAME_WIDTH, "Plus, Minus: Change Volume");
		var t9:FlxText = new FlxText(0, 170, Constants.GAME_WIDTH, "Zero: Mute/Unmute All Audio");
		var t10:FlxText = new FlxText(0, 180, Constants.GAME_WIDTH, "M: Mute/Unmute Music");
		
		var t11:FlxText = new FlxText(0, 200, Constants.GAME_WIDTH, "Your goal is to survive for ten seconds.");
		
		t1.setFormat("fonts/bitrod.ttf", 16, ColorManager.BLACK, "center");
		t2.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		tm.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t3.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t4.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t5.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t6.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t7.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t8.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t9.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t10.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		t11.setFormat("fonts/bitrod.ttf", 8, ColorManager.BLACK, "center");
		
		add(t1);
		add(t2);
		add(tm);
		add(t3);
		add(t4);
		add(t5);
		add(t6);
		add(t7);
		add(t8);
		add(t9);
		add(t10);
		add(t11);
	}
	
	public override function update() {
		super.update();
		
		if (FlxG.keys.justPressed.ANY ) {
			FlxG.switchState(new GameState());
		}
	}
}
