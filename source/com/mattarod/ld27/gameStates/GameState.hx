package com.mattarod.ld27.gameStates;

import com.mattarod.ld27.gameObjects.EnemyGroup;
import com.mattarod.ld27.gameObjects.EnemySprite;
import com.mattarod.ld27.gameObjects.PlayerSprite;
import com.mattarod.ld27.gameObjects.scripts.EarthquakeAction;
import com.mattarod.ld27.gameObjects.scripts.PlaySoundAction;
import com.mattarod.ld27.gameObjects.scripts.Script;
import com.mattarod.ld27.gameObjects.scripts.SetDeadlyAction;
import com.mattarod.ld27.gameObjects.scripts.SetHaltOnGameOverAction;
import com.mattarod.ld27.gameObjects.scripts.SetPositionAction;
import com.mattarod.ld27.gameObjects.scripts.SetVelocityAction;
import com.mattarod.ld27.gameObjects.scripts.SleepAction;
import com.mattarod.ld27.gameObjects.scripts.SleepUntilAction;
import com.mattarod.ld27.gameObjects.scripts.WaitAction;
import com.mattarod.ld27.gameObjects.scripts.WaitUntilAction;
import com.mattarod.ld27.gameObjects.scripts.WildAction;
import com.mattarod.ld27.Registry;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxPoint;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.group.FlxTypedGroup;
import openfl.Assets;

class GameState extends FlxState {
	
	public static inline var TILE_LENGTH:Int = Constants.TILE_LENGTH;
	
	private var _definedSecond:Float;
	function get_definedSecond():Float { return _definedSecond; }
	public var definedSecond(get_definedSecond, null):Float;
	
	private var playerSprite:PlayerSprite;
	private var tilemap:FlxTilemap;
	
	private var countdownText:FlxText;
	private var timeText:FlxText;
	private var deathsText:FlxText;
	private var bestText:FlxText;
	
	private var mode:GameStateMode;
	private var earthquakeDuration:Float;
	private var earthquakeIntensity:Float;
	private var gameOverDuration:Float;
	
	public var enemyGroup:EnemyGroup;
	public var solidObjectGroup:FlxTypedGroup<FlxSprite>;
	
	private var _time:Float = 0;
	function get_time():Float {	return _time; }
	public var time(get_time, null):Float;
	
	function get_gameOver():Bool { return Type.enumEq(mode, GameStateMode.GAMEOVER); }
	public var gameOver(get_gameOver, null):Bool;
	
	public override function create():Void {
		Registry.gameState = this;
		
		_definedSecond = FlxG.elapsed * FlxG.updateFramerate;
		
		solidObjectGroup = new FlxTypedGroup<FlxSprite>();
		enemyGroup = new EnemyGroup();
		add(enemyGroup);
		populateEnemyGroup();
		
		
		playerSprite = new PlayerSprite(10.5 * TILE_LENGTH, 9 * TILE_LENGTH);
		add(playerSprite);
		
		tilemap = new FlxTilemap();
		tilemap.loadMap(Assets.getText("level/lvl1.csv"), "png/tiles.png", TILE_LENGTH, TILE_LENGTH, 1);
		
		add(tilemap);
		
		FlxG.camera.setBounds(TILE_LENGTH, 2 * TILE_LENGTH, 20 * TILE_LENGTH, 15 * TILE_LENGTH);
		
		//FlxG.camera.follow(playerSprite);
		
		countdownText = new FlxText(8 * TILE_LENGTH, 10 * Constants.TILE_LENGTH + 1, 6 * TILE_LENGTH, "READY");
		countdownText.setFormat("fonts/bitrod.ttf", 8, ColorManager.WHITE, "center");
		add(countdownText);
		
		timeText = new FlxText(2 * TILE_LENGTH, 2 * TILE_LENGTH, 5 * TILE_LENGTH, "TIME: 00.00");
		timeText.setFormat("fonts/bitrod-mono.ttf", 8, ColorManager.BLACK, "center");
		add(timeText);
		
		var deaths:Int = Registry.deaths;
		
		var deathString:String = "PLAYS: " +
			(deaths < 1000 ? " " : "") +
			(deaths < 100 ? " " : "") +
			(deaths < 10 ? " " : "") +
			deaths;
		
		deathsText = new FlxText(7 * TILE_LENGTH, 2 * TILE_LENGTH, 8 * TILE_LENGTH, deathString);
		deathsText.setFormat("fonts/bitrod-mono.ttf", 8, ColorManager.BLACK, "center");
		add(deathsText);
		
		var best:Int = Registry.bestTimeInHundrethsOfSeconds;
		var seconds:Int = Math.floor(best / 100);
		var hundredths:Int = best - (seconds * 100);
		
		var bestString:String = "BEST: " +
			(seconds < 10 ? "0" : "") +
			seconds + "." +
			(hundredths < 10 ? "0" : "") +
			hundredths;
		
		bestText = new FlxText(15 * TILE_LENGTH, 2 * TILE_LENGTH, 5 * TILE_LENGTH, bestString);
		bestText.setFormat("fonts/bitrod-mono.ttf", 8, ColorManager.BLACK, "center");
		add(bestText);
		
		mode = GameStateMode.READY;
		_time = - 2 * definedSecond;
		gameOverDuration = definedSecond;
		
		FlxG.sound.play("Ready");
		
		FlxG.sound.playMusic("superlol");
		if (Registry.muteMusic) {
			FlxG.sound.music.volume = 0;
		}
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.M) {
			Registry.muteMusic = !Registry.muteMusic;
			if (Registry.muteMusic) {
				FlxG.sound.music.volume = 0;
			} else {
				FlxG.sound.music.volume = 1;
			}
		}
		
		switch(mode) {
			case READY:
				_time += FlxG.elapsed;
				
				if (_time > -definedSecond) {
					FlxG.sound.play("Ready");
					countdownText.text = "SET";
					mode = GameStateMode.SET;
				}
				
				return;
			
			case SET:
				_time += FlxG.elapsed;
				
				if (_time >= 0) {
					FlxG.sound.play("Go");
					countdownText.text = "GO!";
					_time = 0;
					mode = GameStateMode.GO;
				}
				
				return;
				
			case GAMEOVER:
				super.update();
				
				_time += FlxG.elapsed;
				gameOverDuration -= FlxG.elapsed;
				
				if (gameOverDuration < 0) {
					FlxG.switchState(new MenuState());
				}
				
				return;
				
			case WIN:
				super.update();
				
				countdownText.text = "YOU WIN!";
				timeText.text = "TIME: 10.00";
				
			case GO:
				super.update();
				
				_time += FlxG.elapsed;
				
				if (_time >= 10) {
					mode = GameStateMode.WIN;
					
					return;
				}
		}
		
		var seconds:Int = Math.floor(time);
		var hundredths:Int = Math.round((time - seconds) * 100);
		
		timeText.text = "TIME: " +
			(seconds < 10 ? "0" : "") +
			seconds + "." +
			(hundredths < 10 ? "0" : "") +
			hundredths;
		
		if (earthquakeDuration > 0) {
			earthquakeDuration -= FlxG.elapsed;
			
			FlxG.camera.x = 0;
			FlxG.camera.y = 0;
			
			if (earthquakeDuration > 0) {
				FlxG.camera.x = Math.round((Math.random()-0.5) * earthquakeIntensity);
				FlxG.camera.y = Math.round((Math.random()-0.5) * earthquakeIntensity);
			}
		}
		
		FlxG.collide(playerSprite, tilemap);
		FlxG.collide(playerSprite, solidObjectGroup);
		
		FlxG.overlap(playerSprite, enemyGroup, playerEnemyOverlap);
		
		if (playerSprite.y > 20 * TILE_LENGTH) {
			killPlayer();
		}
	}
	
	private function playerEnemyOverlap(object1:FlxObject, object2:FlxObject):Void {
		
		var enemySprite:EnemySprite = cast(object2);
		
		if (enemySprite.deadly) {
			killPlayer();
		}
	}
	
	public function killPlayer() {
		//playerSprite.exists = false;
		playerSprite.velocity.x = 0;
		playerSprite.velocity.y = 0;
		playerSprite.acceleration.x = 0;
		playerSprite.acceleration.y = 0;
		playerSprite.alive = false;
		playerSprite.solid = false;
		playerSprite.color = ColorManager.BLACK_OPAQUE;
		
		mode = GameStateMode.GAMEOVER;
		countdownText.text = "GAME OVER";
		FlxG.sound.music.stop();
		FlxG.sound.play("Death");
		Registry.deaths++;
		
		var score = Math.round(time * 100);
		if (score > 1000) score = 1000;
		if (score > Registry.bestTimeInHundrethsOfSeconds) Registry.bestTimeInHundrethsOfSeconds = score;
	}
	
	public function earthquake(intensity:Float, duration:Float) {
		earthquakeDuration = duration;
		earthquakeIntensity = intensity;
	}
	
	public function end() {
		closeSubState();
		FlxG.sound.destroy();
		FlxG.switchState(new MenuState());
	}
	
	private function populateEnemyGroup():Void {
		
		var lavaScript:Script = new Script();
		
		lavaScript.addAction(new SleepAction(2.5 * definedSecond));
		lavaScript.addAction(new PlaySoundAction("Lava"));
		lavaScript.addAction(new SetVelocityAction(new FlxPoint(0,  -1.5 * TILE_LENGTH)));
		lavaScript.addAction(new WaitAction(5 * definedSecond));
		
		lavaScript.addAction(new SetPositionAction(new FlxPoint(2 * TILE_LENGTH, 5.5 * TILE_LENGTH)));
		lavaScript.addAction(new SetVelocityAction(new FlxPoint(0,  2 * TILE_LENGTH)));
		lavaScript.addAction(new WaitAction(1.5 * definedSecond));
		
		lavaScript.addAction(new SetPositionAction(new FlxPoint(2 * TILE_LENGTH, 8.5 * TILE_LENGTH)));
		lavaScript.addAction(new SetVelocityAction(new FlxPoint(0,  -2 * TILE_LENGTH)));
		lavaScript.addAction(new WaitAction(definedSecond));
		
		lavaScript.addAction(new SetPositionAction(new FlxPoint(2 * TILE_LENGTH, 6.5 * TILE_LENGTH)));
		lavaScript.addAction(new SetVelocityAction(new FlxPoint(0,  0)));
		lavaScript.addAction(new WaitAction(Script.FOREVER));
		
		var lava = new EnemySprite(2 * TILE_LENGTH, 15 * TILE_LENGTH, 0, 0, true, "png/lava.png", lavaScript);
		lava.shrinkHitbox(16, 0, 0, 0);
		
		enemyGroup.add(lava);
		
		for (i in 2...20) {
			var laserScript1:Script = new Script();
			
			laserScript1.addAction(new SleepAction(0.5 * definedSecond));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.4 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 6 * TILE_LENGTH)));
			laserScript1.addAction(new SleepAction(0.5 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.3 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 12 * TILE_LENGTH)));
			laserScript1.addAction(new SleepAction(0.9 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.4 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 5 * TILE_LENGTH)));
			laserScript1.addAction(new SleepUntilAction(6.0 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 4 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 3 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 2 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 5 * TILE_LENGTH)));
			laserScript1.addAction(new SleepUntilAction(7.2 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.4 * definedSecond));
			laserScript1.addAction(new SleepUntilAction(8.5 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 4 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 3 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 2 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 4 * TILE_LENGTH)));
			laserScript1.addAction(new SleepUntilAction(9.5 * definedSecond));
			
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 3 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 2 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			laserScript1.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 1 * TILE_LENGTH)));
			if (i == 2) laserScript1.addAction(new PlaySoundAction("Laser"));
			laserScript1.addAction(new WaitAction(0.1 * definedSecond));
			
			laserScript1.addAction(new SleepAction(Script.FOREVER));
			
			var laser1:EnemySprite = new EnemySprite(i * TILE_LENGTH, 9 * TILE_LENGTH, 0, 0, true, "png/laser.png", laserScript1);
			
			laser1.shrinkHitbox(2, 2, 0, 0);
			
			enemyGroup.add(laser1);
			
			var laserScript2:Script = new Script();
			
			laserScript2.addAction(new SleepAction(0.9 * definedSecond));
			if (i == 2) laserScript2.addAction(new PlaySoundAction("Laser"));
			laserScript2.addAction(new WaitAction(0.6 * definedSecond));
			laserScript2.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 11 * TILE_LENGTH)));
			laserScript2.addAction(new SleepAction(1.2 * definedSecond));
			if (i == 2) laserScript2.addAction(new PlaySoundAction("Laser"));
			laserScript2.addAction(new WaitAction(0.4 * definedSecond));
			laserScript2.addAction(new SleepAction(Script.FOREVER));
			
			var laser2:EnemySprite = new EnemySprite(i * TILE_LENGTH, 10 * TILE_LENGTH, 0, 0, true, "png/laser.png", laserScript2);
			
			laser2.shrinkHitbox(2, 2, 0, 0);
			
			enemyGroup.add(laser2);
		}
		
		for (i in [8, 13]) {
			var spikeScript1:Script = new Script();
			
			spikeScript1.addAction(new SleepAction(1.3 * definedSecond));
			if (i == 8) spikeScript1.addAction(new PlaySoundAction("Spike"));
			spikeScript1.addAction(new WaitUntilAction(4.5 * definedSecond));
			spikeScript1.addAction(new SleepAction(Script.FOREVER));
			
			var spike:EnemySprite = new EnemySprite(i * TILE_LENGTH, 9 * TILE_LENGTH, 0, 0, true, "png/spikes.png", spikeScript1);
			
			spike.shrinkHitbox(5, 0, 0, 0);
			
			enemyGroup.add(spike);
		}
		
		for (i in [2, 16]) {
			var rockScript2:Script = new Script();
			
			rockScript2.addAction(new SleepAction(1.5 * definedSecond));
			rockScript2.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			if(i == 2) rockScript2.addAction(new PlaySoundAction("Fall"));
			rockScript2.addAction(new WaitAction(4 * definedSecond / 15));
			rockScript2.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript2.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 12 * TILE_LENGTH)));
			if(i == 2) rockScript2.addAction(new PlaySoundAction("Crash"));
			rockScript2.addAction(new SetDeadlyAction(false));
			if(i == 2) rockScript2.addAction(new EarthquakeAction(10, 0.25));
			rockScript2.addAction(new WaitAction(Script.FOREVER));
			
			var rock2:EnemySprite = new EnemySprite(i * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock4x4.png", rockScript2);
			
			enemyGroup.add(rock2);
			solidObjectGroup.add(rock2);
		}
		
		for (i in [6, 14]) {
			var rockScript3:Script = new Script();
			
			rockScript3.addAction(new SleepAction(1.7 * definedSecond));
			rockScript3.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			if(i == 6) rockScript3.addAction(new PlaySoundAction("Fall"));
			rockScript3.addAction(new WaitAction(0.3 * definedSecond));
			rockScript3.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript3.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 14 * TILE_LENGTH)));
			if(i == 6) rockScript3.addAction(new PlaySoundAction("Crash"));
			rockScript3.addAction(new SetDeadlyAction(false));
			if(i == 6) rockScript3.addAction(new EarthquakeAction(10, 0.25));
			rockScript3.addAction(new WaitAction(Script.FOREVER));
			
			var rock3:EnemySprite = new EnemySprite(i * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock2x2.png", rockScript3);
			
			enemyGroup.add(rock3);
			solidObjectGroup.add(rock3);
		}
		
		for (i in [8, 10, 12]) {
			var rockScript4:Script = new Script();
			
			rockScript4.addAction(new SetHaltOnGameOverAction(true));
			if(i == 8) rockScript4.addAction(new WaitAction(2.2 * definedSecond));
			else if (i == 12) rockScript4.addAction(new WaitAction(1.9 * definedSecond));
			else rockScript4.addAction(new WaitAction(4.5 * definedSecond));
			rockScript4.addAction(new SetHaltOnGameOverAction(false));
			rockScript4.addAction(new SetDeadlyAction(true));
			rockScript4.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			rockScript4.addAction(new PlaySoundAction("Fall"));
			rockScript4.addAction(new WaitAction(definedSecond / 15));
			rockScript4.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript4.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 15 * TILE_LENGTH)));
			rockScript4.addAction(new PlaySoundAction("Crash"));
			rockScript4.addAction(new SetDeadlyAction(false));
			rockScript4.addAction(new EarthquakeAction(10, 0.25));
			rockScript4.addAction(new WaitAction(Script.FOREVER));
			
			var rock4:EnemySprite = new EnemySprite(i * TILE_LENGTH, 11 * TILE_LENGTH, 0, 0, false, "png/rock2x1.png", rockScript4);
			
			enemyGroup.add(rock4);
			solidObjectGroup.add(rock4);
		}
		
		for (i in [6, 14]) {
			var rockScript5:Script = new Script();
			
			if (i == 6) rockScript5.addAction(new SleepAction(2.9 * definedSecond));
			else rockScript5.addAction(new SleepAction(3.4 * definedSecond));
			rockScript5.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			rockScript5.addAction(new PlaySoundAction("Fall"));
			rockScript5.addAction(new WaitAction(4 * definedSecond / 15));
			rockScript5.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript5.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 12 * TILE_LENGTH)));
			rockScript5.addAction(new PlaySoundAction("Crash"));
			rockScript5.addAction(new SetDeadlyAction(false));
			rockScript5.addAction(new EarthquakeAction(10, 0.25));
			rockScript5.addAction(new WaitAction(Script.FOREVER));
			
			var rock5:EnemySprite = new EnemySprite(i * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock2x2.png", rockScript5);
			
			enemyGroup.add(rock5);
			solidObjectGroup.add(rock5);
		}
		
		for (i in [6, 14]) {
			var rockScript6:Script = new Script();
			
			if (i == 6) rockScript6.addAction(new SleepAction(3.0 * definedSecond));
			else rockScript6.addAction(new SleepAction(3.9 * definedSecond));
			rockScript6.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			rockScript6.addAction(new PlaySoundAction("Fall"));
			rockScript6.addAction(new WaitAction(7 * definedSecond / 30));
			rockScript6.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript6.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 10 * TILE_LENGTH)));
			rockScript6.addAction(new PlaySoundAction("Crash"));
			rockScript6.addAction(new SetDeadlyAction(false));
			rockScript6.addAction(new EarthquakeAction(10, 0.25));
			rockScript6.addAction(new WaitAction(Script.FOREVER));
			
			var rock6:EnemySprite = new EnemySprite(i * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock2x2.png", rockScript6);
			
			enemyGroup.add(rock6);
			solidObjectGroup.add(rock6);
		}
		
		for (i in [4, 16]) {
			var rockScript8:Script = new Script();
			
			if (i == 4) rockScript8.addAction(new SleepAction(5.5 * definedSecond));
			else rockScript8.addAction(new SleepAction(7.7 * definedSecond));
			rockScript8.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
			rockScript8.addAction(new PlaySoundAction("Fall"));
			rockScript8.addAction(new WaitAction(definedSecond / 6));
			rockScript8.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			rockScript8.addAction(new SetPositionAction(new FlxPoint(i * TILE_LENGTH, 8 * TILE_LENGTH)));
			rockScript8.addAction(new PlaySoundAction("Crash"));
			rockScript8.addAction(new SetDeadlyAction(false));
			rockScript8.addAction(new EarthquakeAction(10, 0.25));
			rockScript8.addAction(new WaitAction(Script.FOREVER));
			
			var rock8:EnemySprite = new EnemySprite(i * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock2x2.png", rockScript8);
			
			enemyGroup.add(rock8);
			solidObjectGroup.add(rock8);
		}
		
		for (i in [2, 3, 4, 5, 16, 17, 18, 19]) {
			var spikeScript2:Script = new Script();
			
			spikeScript2.addAction(new SleepAction(3.8 * definedSecond));
			if (i == 3) spikeScript2.addAction(new PlaySoundAction("Spike"));
			spikeScript2.addAction(new WaitAction(Script.FOREVER));
			
			var spike2:EnemySprite = new EnemySprite(i * TILE_LENGTH, 11 * TILE_LENGTH, 0, 0, true, "png/spikes.png", spikeScript2);
			
			spike2.shrinkHitbox(5, 0, 0, 0);
			
			enemyGroup.add(spike2);
		}
		
		for (i in [4, 5, 16, 17]) {
			var spikeScript3:Script = new Script();
			
			if (i < 10) spikeScript3.addAction(new SleepAction(7.0 * definedSecond));
			else spikeScript3.addAction(new SleepAction(9.0 * definedSecond));
			if (i == 5 || i == 16) spikeScript3.addAction(new PlaySoundAction("Spike"));
			spikeScript3.addAction(new WaitAction(Script.FOREVER));
			
			var spike3:EnemySprite = new EnemySprite(i * TILE_LENGTH, 7 * TILE_LENGTH, 0, 0, true, "png/spikes.png", spikeScript3);
			
			spike3.shrinkHitbox(5, 0, 0, 0);
			
			enemyGroup.add(spike3);
		}
		
		for (i in [9, 10, 11, 12]) {
			var spikeScript4:Script = new Script();
			
			var extraTime:Float = (13 - i) * 0.1 * definedSecond;
			
			spikeScript4.addAction(new SleepAction((9.5 * definedSecond) + extraTime));
			spikeScript4.addAction(new PlaySoundAction("Spike"));
			spikeScript4.addAction(new WaitUntilAction(10 * definedSecond));
			if(i == 9) spikeScript4.addAction(new PlaySoundAction("Spike"));
			spikeScript4.addAction(new SleepAction(Script.FOREVER));
			
			var spike4:EnemySprite = new EnemySprite(i * TILE_LENGTH, 5 * TILE_LENGTH, 0, 0, true, "png/spikes.png", spikeScript4);
			
			spike4.shrinkHitbox(5, 0, 0, 0);
			
			enemyGroup.add(spike4);
		}
		
		var barScript:Script = new Script();
		
		barScript.addAction(new SetHaltOnGameOverAction(true));
		barScript.addAction(new WaitAction(4.5 * definedSecond));
		barScript.addAction(new SetHaltOnGameOverAction(false));
		barScript.addAction(new SetDeadlyAction(true));
		barScript.addAction(new SetVelocityAction(new FlxPoint(0, 30 * TILE_LENGTH / definedSecond)));
		barScript.addAction(new WaitAction(definedSecond / 15));
		barScript.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
		barScript.addAction(new SetPositionAction(new FlxPoint(8 * TILE_LENGTH, 14 * TILE_LENGTH)));
		barScript.addAction(new SetDeadlyAction(false));
		
		barScript.addAction(new WildAction(
			function(a:EnemySprite) {
				countdownText.y += 4 * TILE_LENGTH;
			}
		));
		
		barScript.addAction(new WaitUntilAction(5.0 * definedSecond));
		barScript.addAction(new PlaySoundAction("BigCrash"));
		
		barScript.addAction(new WildAction(
			function(a:EnemySprite) {
				timeText.y -= 2 * TILE_LENGTH;
				deathsText.y -= 2 * TILE_LENGTH;
				bestText.y -= 2 * TILE_LENGTH;
				
				FlxG.camera.setBounds(TILE_LENGTH, 0, 20 * TILE_LENGTH, 15 * TILE_LENGTH);
				playerSprite.y -= 2 * TILE_LENGTH;
				lava.y -= 2 * TILE_LENGTH;
			}
		));
		
		for (i in [-2, 20]) {
			var direction:Int = i > 0 ? -1 : 1;
			var destinationX:Float = (i + direction * 4) * TILE_LENGTH;
			
			var smasherScript:Script = new Script();
			
			smasherScript.addAction(new SleepAction(4.0));
			smasherScript.addAction(new SetVelocityAction(new FlxPoint(32 * TILE_LENGTH * direction, 0)));
			smasherScript.addAction(new WaitAction(definedSecond / 8));
			smasherScript.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
			smasherScript.addAction(new SetPositionAction(new FlxPoint(destinationX, 10 * TILE_LENGTH)));
			if(i == -2) smasherScript.addAction(new PlaySoundAction("Crash"));
			smasherScript.addAction(new SetDeadlyAction(false));
			if(i == -2) smasherScript.addAction(new EarthquakeAction(10, 0.25));
			smasherScript.addAction(new WaitAction(Script.FOREVER));
			
			var smasher:EnemySprite = new EnemySprite(i * TILE_LENGTH, 10 * TILE_LENGTH, 0, 0, true, "png/smasher.png", smasherScript);
			
			enemyGroup.add(smasher);
			solidObjectGroup.add(smasher);
		}
		
		barScript.addAction(new WaitAction(Script.FOREVER));
		
		var bar:EnemySprite = new EnemySprite(8 * TILE_LENGTH, 10 * TILE_LENGTH, 0, 0, false, "png/bar.png", barScript);
		solidObjectGroup.add(bar);
		enemyGroup.add(bar);
		
		var rockScript1:Script = new Script();
		
		rockScript1.addAction(new SleepAction(0.2 * definedSecond));
		rockScript1.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
		rockScript1.addAction(new PlaySoundAction("Fall"));
		rockScript1.addAction(new WaitAction(definedSecond / 6));
		rockScript1.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
		rockScript1.addAction(new SetPositionAction(new FlxPoint(9 * TILE_LENGTH, 6 * TILE_LENGTH)));
		rockScript1.addAction(new PlaySoundAction("Crash"));
		rockScript1.addAction(new SetDeadlyAction(false));
		rockScript1.addAction(new EarthquakeAction(10, 0.25));
		rockScript1.addAction(new SetHaltOnGameOverAction(true));
		rockScript1.addAction(new WaitUntilAction(4.5 * definedSecond));
		rockScript1.addAction(new SetHaltOnGameOverAction(false));
		rockScript1.addAction(new SetVelocityAction(new FlxPoint(0, 30 * TILE_LENGTH / definedSecond)));
		rockScript1.addAction(new WaitAction(definedSecond / 15));
		rockScript1.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
		rockScript1.addAction(new SetPositionAction(new FlxPoint(9 * TILE_LENGTH, 10 * TILE_LENGTH)));
		rockScript1.addAction(new WaitAction(Script.FOREVER));
		
		var rock1:EnemySprite = new EnemySprite(9 * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock4x4.png", rockScript1);
		
		enemyGroup.add(rock1);
		solidObjectGroup.add(rock1);
		
		var rockScript7:Script = new Script();
		
		rockScript7.addAction(new SleepAction(4.0 * definedSecond));
		rockScript7.addAction(new SetVelocityAction(new FlxPoint(0, 60 * TILE_LENGTH / definedSecond)));
		rockScript7.addAction(new PlaySoundAction("Fall"));
		rockScript7.addAction(new WaitAction(definedSecond / 10));
		rockScript7.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
		rockScript7.addAction(new SetPositionAction(new FlxPoint(9 * TILE_LENGTH, 2 * TILE_LENGTH)));
		rockScript7.addAction(new PlaySoundAction("Crash"));
		rockScript7.addAction(new SetDeadlyAction(false));
		rockScript7.addAction(new EarthquakeAction(10, 0.25));
		rockScript7.addAction(new WaitUntilAction(4.5 * definedSecond));
		rockScript7.addAction(new SetVelocityAction(new FlxPoint(0, 30 * TILE_LENGTH / definedSecond)));
		rockScript7.addAction(new WaitAction(definedSecond / 15));
		rockScript7.addAction(new SetVelocityAction(new FlxPoint(0, 0)));
		rockScript7.addAction(new SetPositionAction(new FlxPoint(9 * TILE_LENGTH, 6 * TILE_LENGTH)));
		rockScript7.addAction(new PlaySoundAction("BigCrash"));
		rockScript7.addAction(new EarthquakeAction(20, 1.0));
		rockScript7.addAction(new WaitAction(Script.FOREVER));
		
		var rock7:EnemySprite = new EnemySprite(9 * TILE_LENGTH, -4 * TILE_LENGTH, 0, 0, true, "png/rock4x4.png", rockScript7);
		
		enemyGroup.add(rock7);
		solidObjectGroup.add(rock7);
		
	}
}
