package com.mattarod.ld27.gameStates;

enum GameStateMode {
	READY;
	SET;
	GO;
	GAMEOVER;
	WIN;
}
